# Build a Canadian Cross Compiler targeting ARM microcontrollers

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# It is not easy to build this toolchain natively on the Raspberry Pi, because of how
# much memory and disk space is required to build GCC.  Using the Candian Cross
# technique, we can build the toolchain on an x86 Linux system (build machine) to
# run on the Raspberry Pi (host machine) to generate code for an ARM microcontroller
# (target machine).

# Host machine (where we want to run the cross-compiler) definitions

HOST_PLATFORM_NAME		= RaspberryPi
HOST_OSNAME			= rpi
HOST_CONFIG_NAME		= --host=arm-linux-gnueabihf
HOST_PKGARCH			= armhf
HOST_PKGSUFFIX			= $(HOST_OSNAME)-$(HOST_PKGARCH)

# Build machine (where we want to build the cross-compiler) definitions

BUILD_CONFIG_NAME		= --build=`gcc -v 2>&1 | awk '/Target:/ { print $$2 }'`
BUILD_HOST_TOOLCHAIN_DIR	= /usr/local/gcc-arm-linux-gnueabihf-linaro-$(HOST_PLATFORM_NAME)/bin
BUILD_MCU_TOOLCHAIN_DIR		= /usr/local/arm-mcu-tools/bin

# The following make flags enable the Canadian Cross Compiler build

CROSSMAKEFLAGS			:= PATH="$(PATH):$(BUILD_HOST_TOOLCHAIN_DIR):$(BUILD_MCU_TOOLCHAIN_DIR)" BUILDCONFIG="$(BUILD_CONFIG_NAME) $(HOST_CONFIG_NAME)" OSNAME=$(HOST_OSNAME) PKGARCH=$(HOST_PKGARCH) PKGSUFFIX=$(HOST_PKGSUFFIX)

# The following build targets are available

default:
	$(MAKE) $@ $(CROSSMAKEFLAGS)

download:
	$(MAKE) $@ $(CROSSMAKEFLAGS)

source.done:
	$(MAKE) $@ $(CROSSMAKEFLAGS)

tarball:
	$(MAKE) $@ $(CROSSMAKEFLAGS)

package.deb:
	$(MAKE) $@ $(CROSSMAKEFLAGS)

clean:
	$(MAKE) $@ $(CROSSMAKEFLAGS)

distclean:
	$(MAKE) $@ $(CROSSMAKEFLAGS)
