# STM32F4 family board specific make definitions

# Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Board specific macro definitions

ifeq ($(BOARDNAME), CERB40)
MCU		= stm32f405rg
BOARDFLAGS	+= -DSTM32F405xx -DHSE_PLL -DHSE_VALUE=12000000
BOARDFLAGS	+= -DLED1_PIN=GPIOPIN21 -DLED1_OUTPUT=GPIOPIN21OUT
CONSOLEPORT	?= com2:115200,n,8,1
CONSOLEFLAGS	?= -DCONSOLE_SERIAL -DCONSOLE_PORT='"$(CONSOLEPORT)"'
endif

ifeq ($(BOARDNAME), NETDUINOPLUS2)
MCU		= stm32f405rg
BOARDFLAGS	+= -DSTM32F405xx -DHSE_PLL -DHSE_VALUE=25000000
BOARDFLAGS	+= -DLED1_PIN=GPIOPIN10 -DLED1_OUTPUT=GPIOPIN10OUT
CONSOLEPORT	?= com2:115200,n,8,1
CONSOLEFLAGS	?= -DCONSOLE_SERIAL -DCONSOLE_PORT='"$(CONSOLEPORT)"'
# Auxiliary serial port is USART6 on Ardunio D0 and D1
AUXPORT		?= com6:115200,n,8,1
BOARDFLAGS	+= -DAUX_PORT='"$(AUXPORT)"'
endif

ifeq ($(BOARDNAME), NUCLEO_F411RE)
MCU		= stm32f411re
BOARDFLAGS	+= -DSTM32F411xE -DHSE_PLL -DHSE_VALUE=8000000 -DHSE_BYPASS
BOARDFLAGS	+= -DBUTTON1_PIN=GPIOPIN45 -DBUTTON1_INPUT=GPIOPIN45IN -DBUTTON1_ACTIVELOW
BOARDFLAGS	+= -DLED1_PIN=GPIOPIN5 -DLED1_OUTPUT=GPIOPIN5OUT
CONSOLEPORT	?= com2:115200,n,8,1
CONSOLEFLAGS	?= -DCONSOLE_SERIAL -DCONSOLE_PORT='"$(CONSOLEPORT)"'
# Auxiliary serial port is USART6 on Ardunio D0 and D1 -- Must wire wrap
# CN10 pin 14 (PA11, TxD) to CN10 pin 35 (D1) and
# CN10 pin 12 (PA12, RxD) to CN10 pin 37 (D0)
AUXPORT		?= com6:115200,n,8,1
BOARDFLAGS	+= -DAUX_PORT='"$(AUXPORT)"'
ifeq ($(shell uname), Darwin)
MBEDDIR		?= /Volumes/NODE_F411RE
endif
ifeq ($(shell uname), Linux)
MBEDDIR		?= /media/$(USER)/NODE_F411RE
endif
OPENOCDIF	= stlink-v2-1
endif

ifeq ($(BOARDNAME), NUCLEO_F446RE)
MCU		= stm32f446re
BOARDFLAGS	+= -DSTM32F446xx -DHSE_PLL -DHSE_VALUE=8000000 -DHSE_BYPASS
BOARDFLAGS	+= -DBUTTON1_PIN=GPIOPIN45 -DBUTTON1_INPUT=GPIOPIN45IN -DBUTTON1_ACTIVELOW
BOARDFLAGS	+= -DLED1_PIN=GPIOPIN5 -DLED1_OUTPUT=GPIOPIN5OUT
CONSOLEPORT	?= com2:115200,n,8,1
CONSOLEFLAGS	?= -DCONSOLE_SERIAL -DCONSOLE_PORT='"$(CONSOLEPORT)"'
# Auxiliary serial port is USART5 on Ardunio D0 and D1 -- Must wire wrap
# CN7 pin 3 (PC12, TxD) to CN10 pin 35 (D1) and
# CN7 pin 4 (PD2, RxD)  to CN10 pin 37 (D0)
AUXPORT		?= com5:115200,n,8,1
BOARDFLAGS	+= -DAUX_PORT='"$(AUXPORT)"'
ifeq ($(shell uname), Darwin)
MBEDDIR		?= /Volumes/NODE_F446RE
endif
ifeq ($(shell uname), Linux)
MBEDDIR		?= /media/$(USER)/NODE_F446RE
endif
OPENOCDIF	= stlink-v2-1
endif

ifeq ($(BOARDNAME), STM32F4_DISCOVERY)
MCU		= stm32f407vg
BOARDFLAGS	+= -DSTM32F407xx -DHSE_PLL -DHSE_VALUE=8000000
BOARDFLAGS	+= -DBUTTON1_PIN=GPIOPIN0 -DBUTTON1_INPUT=GPIOPIN0IN
BOARDFLAGS	+= -DLED1_PIN=GPIOPIN60 -DLED1_OUTPUT=GPIOPIN60OUT
BOARDFLAGS	+= -DLED2_PIN=GPIOPIN61 -DLED2_OUTPUT=GPIOPIN61OUT
BOARDFLAGS	+= -DLED3_PIN=GPIOPIN62 -DLED3_OUTPUT=GPIOPIN62OUT
BOARDFLAGS	+= -DLED4_PIN=GPIOPIN63 -DLED4_OUTPUT=GPIOPIN63OUT
CONSOLEPORT	?= com2:115200,n,8,1
CONSOLEFLAGS	?= -DCONSOLE_SERIAL -DCONSOLE_PORT='"$(CONSOLEPORT)"'
# Auxiliary serial port is USART3 (Discovery Shield Mikrobus sockets 1 or 2)
AUXPORT		?= com3:115200,n,8,1
# Or possibly USART6 (Discovery Shield Mikrobus sockets 3 or 4)
#AUXPORT	?= com6:115200,n,8,1
BOARDFLAGS	+= -DAUX_PORT='"$(AUXPORT)"'
endif

ifeq ($(BOARDNAME), STM32_M4_MINI)
MCU		= stm32f415rg
BOARDFLAGS	+= -DSTM32F415xx -DHSE_PLL -DHSE_VALUE=16000000
BOARDFLAGS	+= -DLED1_PIN=GPIOPIN44 -DLED1_OUTPUT=GPIOPIN44OUT
BOARDFLAGS	+= -DLED2_PIN=GPIOPIN45 -DLED2_OUTPUT=GPIOPIN45OUT
CONSOLEPORT	?= com2:115200,n,8,1
CONSOLEFLAGS	?= -DCONSOLE_SERIAL -DCONSOLE_PORT='"$(CONSOLEPORT)"'
endif

ifeq ($(BOARDNAME), STM32_M4_CLICKER)
MCU		= stm32f415rg
BOARDFLAGS	+= -DSTM32F415xx -DHSE_PLL -DHSE_VALUE=16000000
BOARDFLAGS	+= -DBUTTON1_PIN=GPIOPIN32 -DBUTTON1_INPUT=GPIOPIN32IN -DBUTTON1_ACTIVELOW
BOARDFLAGS	+= -DBUTTON2_PIN=GPIOPIN33 -DBUTTON2_INPUT=GPIOPIN33IN -DBUTTON2_ACTIVELOW
BOARDFLAGS	+= -DLED1_PIN=GPIOPIN1 -DLED1_OUTPUT=GPIOPIN1OUT
BOARDFLAGS	+= -DLED2_PIN=GPIOPIN2 -DLED2_OUTPUT=GPIOPIN2OUT
CONSOLEPORT	?= com1:115200,n,8,1
CONSOLEFLAGS	?= -DCONSOLE_SERIAL -DCONSOLE_PORT='"$(CONSOLEPORT)"'
# Auxiliary serial port is USART3, on the Mikrobus socket
AUXPORT		?= com3:115200,n,8,1
BOARDFLAGS	+= -DAUX_PORT='"$(AUXPORT)"'
endif
